class Following < ActiveRecord::Base

  default_scope order("created_at DESC")

  scope :active, where(:is_canceled => false)
  scope :canceled, where(:is_canceled => true)

  belongs_to :term, :counter_cache => true
  attr_accessible :term_run_at, :screen_name, :tweet, :user_data

  serialize :user_data
  serialize :tweet
end
