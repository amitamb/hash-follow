class User < ActiveRecord::Base

  has_many :terms

  attr_accessible :email, :notify

  serialize :auth_hash

  validates :email,
            :format => {  :with => /^([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})$/i,
                          :message => "is invalid" },
            :allow_blank => true
            #:presence => true,
            #:uniqueness => true,
            #:unless => "new_record?"

  def self.from_omniauth(auth)



    user = where(auth.slice("provider", "uid")).first || create_from_omniauth(auth)
    if user.auth_hash.blank?
      user.auth_hash ||= auth.to_hash
      puts "I saved it"
      user.save!
    end
    user
  end

  def lang
    (auth_hash["extra"] && auth_hash["extra"]["raw_info"] && auth_hash["extra"]["raw_info"]["lang"]) || nil
  end

  def self.create_from_omniauth(auth)
    create! do |user|
      user.provider = auth["provider"]
      user.uid = auth["uid"]
      user.name = auth["info"]["nickname"]
      if auth['credentials']
        user.access_token        = auth['credentials']['token']
        user.access_token_secret = auth['credentials']['secret']
      end
    end
  end

  def self.run_search_and_follow_task
    User.all.each do |user|
      Resque.enqueue(UserFollower, user.id)
    end
  end

  def self.run_unfollow_nonfollowers
    User.all.each do |user|
      Resque.enqueue(UserFollower, user.id)
    end
  end
end
