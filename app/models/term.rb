class Term < ActiveRecord::Base

  DAILY = 1
  TWICE_DAILY = 2
  HOURLY = 3
  WEEKLY = 4

  def self.interval_map
    {
      "Daily" => DAILY#,
      #"Twice Daily" => TWICE_DAILY,
      #"Hourly" => HOURLY,
      #"Weekly"=> WEEKLY
    }
  end

  scope :disabled, where(is_disabled: true)
  scope :enabled, where(is_disabled: false)

  validates_presence_of :name, :max_to_follow
  validates :max_to_follow, :inclusion => { :in => [30, 20, 10, 5],
    :message => "%{value} is not valid" }
  validates :name, :uniqueness => { :scope => :user_id, :message => "has already been added" }

  has_many :followings
  belongs_to :user
  attr_accessible :interval, :max_to_follow, :name, :notify, :run_now

  attr_accessor :run_now

  def run_now
    self[:run_now] || true
  end

  after_save :run_now_check

  def run_now_check
    if run_now
      self.run_task!
    end
    true
  end

  MAX_ATTEMPTS = 3

  def run_task!
    Resque.enqueue(TermFollower, self.id)
  end

  def disable!
    self.update_attribute(:is_disabled, true)
  end

  def enable!
    self.update_attribute(:is_disabled, false)
  end

  def self.follow_scheduled_terms
    Term.enabled.all.each do |term|
      Resque.enqueue(TermFollower, term.id)
    end
  end

  def followings_a_week_ago
    self.followings.where("created_at < :week", {:week => 1.week.ago}).active
  end
end
