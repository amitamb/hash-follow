ActiveAdmin.register_page "Dashboard" do

  menu :priority => 1, :label => proc{ I18n.t("active_admin.dashboard") }

  content :title => proc{ I18n.t("active_admin.dashboard") } do

    div :style => "margin:5px 0 15px 0;font-size:16px;" do
      span :style => "" do
        span link_to "See Resque Dashboard", "/resque_password_protected_2902213"
      end
    end
    
    columns do

      column do
        panel "Recent Users" do
          table_for User.limit(10).order('created_at desc').limit(10) do
            column("Id")   {|user| link_to(user.id, admin_user_path(user)) } 
            column("Name")   {|user| link_to(user.name, "http://twitter.com/" + user.name, :target => "_blank") } 
            column("Email"){|user| user.email || "Not provided" } 
            column("Date/Time")   {|user| time_ago_in_words(user.created_at) + " ago" } 
            column("Terms added") {|user|user.terms.count}
          end
        end
      end

      column do
        panel "Recent Terms" do
          table_for Term.order('created_at desc').limit(10).each do |term|
            column(:id)      {|term| link_to(term.id, admin_term_path(term)) }
            column(:name)    {|term| link_to(term.name, "https://twitter.com/search?q=" + CGI::escape(term.name), :target => "_blank") }
            column(:followings_count)    {|term| term.followings.count }
            #column(:user)    {|term| link_to(term.user.name, admin_user_path(term.user)) }
            column("Last Run")   {|term| time_ago_in_words(term.updated_at) + " ago" } 
          end
        end
      end

    end # columns
  end # content
end
