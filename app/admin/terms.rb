ActiveAdmin.register Term do
  member_action :run, :method => :get do
    term = Term.find(params[:id])
    term.run_task!
    redirect_to :back
  end

  index do
    column :id
    column :name
    column :followings do |term|
      term.followings.count
    end
    column :interval
    column :max_to_follow
    column :notify
    column :last_run do |term|
      if term.last_run_at
        time_ago_in_words(term.last_run_at) + " ago"
      else
        "never"
      end
    end

    column "Actions" do |term|
      actions =  link_to("Run Now", run_admin_term_path(term), :method => :get) + " "
      actions += link_to("View", admin_term_path(term)) + " "
      actions += link_to("Edit", edit_admin_term_path(term)) + " "
      actions += link_to("Delete", admin_term_path(term), :method => :delete)
      actions
    end
  end
end
