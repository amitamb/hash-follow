class TermFollower

  @queue = :terms_queue

  MAX_ATTEMPTS = 3

  def self.perform(term_id)
    self.follow(Term.find(term_id))
  end

  def self.follow(term, notify = false)

    client = Twitter::Client.new(
      :oauth_token => term.user.access_token,
      :oauth_token_secret => term.user.access_token_secret
    )

    results = []
    rate_limit_immune do
      results = client.search("#{term.name} -rt", :count => term.max_to_follow).results
    end

    screen_name_to_result_map = {}
    results.reverse.map { |r| # doing reverse so oldest tweet user gets followed first
      if r[:user][:lang] == "en" || r[:user][:lang] == term.user.lang
        screen_name_to_result_map[r[:user][:screen_name]] = r
      else
        puts "Can not follow " + r[:user][:screen_name] + " as not english and did not match user's lang"
      end
    }

    followings = []
    existing_friend_ids = []
    rate_limit_immune do
      existing_friend_ids = client.friend_ids.to_a
    end

    term_run_time = Time.now

    term.update_attribute(:last_run_at, term_run_time)

    screen_name_to_result_map.each do |screen_name, result|
      rate_limit_immune do
        puts "Trying to follow user : " + screen_name.to_s + " for term #{term.name} from #{term.user.email}"
        status = client.follow!(screen_name, :follow => true)

        new_following = false
        if status.present?
          if !existing_friend_ids.include?(status[0][:id])
            new_following = true
          end
        end

        if new_following
          following = term.followings.create(:term_run_at => term_run_time, :screen_name => screen_name, :tweet => result)
          followings << following
        end

        puts "Followed user : " + screen_name
        sleep 1.0/5.0
      end
   end

   followings

  end

  def self.unfollow(term, notify = false)

    client = Twitter::Client.new(
      :oauth_token => term.user.access_token,
      :oauth_token_secret => term.user.access_token_secret
    )

    followings = term.followings_a_week_ago
    existing_follower_ids = []
    rate_limit_immune do
      existing_follower_ids = client.follower_ids.to_a
    end

    # existing_friend_ids = []
    # rate_limit_immune do
    #   existing_friend_ids = client.friend_ids.to_a
    # end

    followings.each do |following|

      if !existing_follower_ids.include?(following.tweet.user.id)
        rate_limit_immune do
          puts "Unfollowing " + following.screen_name
          status = client.unfollow(following.screen_name)

          sleep 1.0/5.0
        end
      end

   end

   followings

  end

  private

  def self.rate_limit_immune
    num_attempts = 0
    begin
      num_attempts += 1
      yield
    rescue Twitter::Error::TooManyRequests => error
      if num_attempts <= MAX_ATTEMPTS
        sleep error.rate_limit.reset_in
        retry
      else
        raise
      end
    end
  end

end