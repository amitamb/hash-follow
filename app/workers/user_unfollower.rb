class UserUnfollower

  @queue = :users_queue

  MAX_ATTEMPTS = 3

  def self.perform(user_id)
    user = User.find(user_id)

    term_followings = []

    total_terms = 0
    total_followings = 0

    user.terms.enabled.each do |term|
      followings = TermFollower.unfollow(term)
    end

    if total_followings > 0 && user.notify?
      UserMailer.followings_notify(user, term_followings, total_terms, total_followings).deliver
    end
  end
end