class UserFollower

  @queue = :users_queue

  MAX_ATTEMPTS = 3

  def self.perform(user_id)
    user = User.find(user_id)

    term_followings = []

    total_terms = 0
    total_followings = 0

    user.terms.enabled.each do |term|
      followings = TermFollower.follow(term)
      if term.notify?
        term_followings << { term: term, followings: followings }
        total_terms += 1
        total_followings += followings.count
      end
    end

    if total_followings > 0 && user.notify?
      UserMailer.followings_notify(user, term_followings, total_terms, total_followings).deliver
    end
  end
end