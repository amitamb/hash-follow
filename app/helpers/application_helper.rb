module ApplicationHelper
  def format_tweet(text)
    text = text.gsub(/\@([^ \,]+)\b/, '<a href="https://twitter.com/\1" target="_blank">@\1</a>').html_safe
    text = text.gsub(/\#([^ \,]+)\b/, '<a href="https://twitter.com/search?q=%23\1&src=hash" target="_blank">#\1</a>').html_safe
    text = Rinku.auto_link(text, mode=:all, link_attr=nil, skip_tags=nil).html_safe
  end
end
