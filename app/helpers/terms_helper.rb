module TermsHelper
  def can_add_term?
    current_user.terms.count < 3
  end

  def last_run_time(term)
    term.last_run_at.present? ? time_ago_in_words(term.last_run_at) + " ago" : "never"
  end
end
