
class TermsController < ApplicationController

  before_filter :login_required
  before_filter :terms_count_limit, :only => [:new, :create]

  # GET /terms
  # GET /terms.json
  def index
    @terms = current_user.terms

    respond_to do |format|
      format.html {
        if @terms.count <= 0 
          redirect_to new_term_path
        end
      }
      format.json { render json: @terms }
    end
  end

  # GET /terms/1
  # GET /terms/1.json
  def show
    @term = current_user.terms.find(params[:id])
    @followings = @term.followings.page(params[:page] || 0).per(10)

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @term }
    end
  end

  # GET /terms/new
  # GET /terms/new.json
  def new
    @term = current_user.terms.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @term }
    end
  end

  # GET /terms/1/edit
  def edit
    @term = current_user.terms.find(params[:id])
  end

  # POST /terms
  # POST /terms.json
  def create
    @term = current_user.terms.new(params[:term])

    respond_to do |format|
      if @term.save
        format.html { redirect_to @term, notice: 'Term was successfully created.' }
        format.json { render json: @term, status: :created, location: @term }
      else
        format.html { render action: "new" }
        format.json { render json: @term.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /terms/1
  # PUT /terms/1.json
  def update
    @term = current_user.terms.find(params[:id])

    respond_to do |format|
      if @term.update_attributes(params[:term])
        format.html { redirect_to @term, notice: 'Term was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @term.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /terms/1
  # DELETE /terms/1.json
  def destroy
    @term = current_user.terms.find(params[:id])
    @term.destroy

    respond_to do |format|
      format.html { redirect_to terms_url }
      format.json { head :no_content }
    end
  end

  def disable
    @term = Term.find(params[:id])
    @term.disable!

    respond_to do |format|
      format.js
      format.html {
        redirect_to :back, :notice => "Disabled the term."
      }
      format.json { render json: @term }
    end
  end

  def enable
    @term = Term.find(params[:id])
    @term.enable!

    respond_to do |format|
      format.js
      format.html {
        redirect_to :back, :notice => "Enabled the term."
      }
      format.json { render json: @term }
    end
  end

  def run
    @term = Term.find(params[:id])
    @term.run_task!

    respond_to do |format|
      format.js
      format.html {
        redirect_to :back, :notice => "Successfully scheduled the search and follow task."
      }
      format.json { render json: @term }
    end
  end

  def log

  end

  private

  def terms_count_limit
    if current_user.terms.count >= 3
      redirect_to root_url, notice: "Can create only 3 terms with free plan. <a href='/plans'>Sing up for premium membership</a>"
    end
  end
end