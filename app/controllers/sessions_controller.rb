class SessionsController < ApplicationController
  def create
    user = User.from_omniauth(env["omniauth.auth"])
    session[:user_id] = user.id
    to_url = root_url
    if session[:login_to].present?
      to_url = session[:login_to]
      session[:login_to] = nil
    end
    redirect_to to_url, notice: "Signed in!"
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url, notice: "Signed out!"
  end
end
