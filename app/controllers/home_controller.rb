
class HomeController < ApplicationController
  def index
  	if current_user
      if current_user.email.present?
        redirect_to terms_path
      else
        redirect_to "/user/edit", notice: "Please update your email address."
      end
    else
      render "index"
    end
  end

  def search
    @client = Twitter::Client.new(
      :oauth_token => current_user.access_token,
      :oauth_token_secret => current_user.access_token_secret
    )

    @q = params[:q]
    if @q.present?
      @results = @client.search(@q + " -rt", :count => 30).results
    else
      @results = []
    end
    respond_to do |format|
      format.js
    end
  end

  def why
    
  end

  def del
    #@result = Twitter.follow("starksean")

    @result = Twitter.search("@amitamb -rt", :count => 10).results

    Twitter.follow("Microki")

    @result.each do |result|
      screen_name = result[:user][:screen_name]
      #Twitter.follow(screen_name)
    end
  end

  def test
    render layout: nil
  end
end
