$ ->
  if ( $("#term_name").length > 0 )
    search_tweets = $("<a href='#' style='margin-left:5px;'>Search</a>")
    search_tweets.click ->
      $(".tweeter_results table").html("<tr><td style='padding:10px;text-align:center;vertical-align:middle;'>Searching...</td></tr>")
      $.get "/home/search", 
        q: $("#term_name").val()
      return false
    $("#term_name").after(search_tweets);
    $("#term_interval").next(".help-block").html("Can run only daily in free version <a href='/plans'>See plans</a>");