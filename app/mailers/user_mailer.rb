class UserMailer < ActionMailer::Base

  add_template_helper(ApplicationHelper)

  default from: "hashfollow@verticalset.com"

  def followings_notify(user, term_followings, total_terms, total_followings)
    @user = user
    @term_followings = term_followings
    @total_followings = total_followings
    @total_terms = total_terms

    mail to: user.email, subject: "HashFollow Recent Activity.."
  end
end
