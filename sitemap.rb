require 'rubygems'
require 'sitemap_generator'

SitemapGenerator::Sitemap.default_host = 'http://www.hashfollow.com'
SitemapGenerator::Sitemap.create do
  add '/', :changefreq => 'daily', :priority => 0.9
  add '/why', :changefreq => 'weekly'
  add '/who', :changefreq => 'weekly'
  add '/contact', :changefreq => 'weekly'
  add '/plans', :changefreq => 'weekly'
end
SitemapGenerator::Sitemap.ping_search_engines # called for you when you use the rake task