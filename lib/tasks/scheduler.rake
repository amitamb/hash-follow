desc "Scheduler will call this task daily to send deals related to upcoming events"
task :follow_terms => :environment do
  #Term.follow_scheduled_terms
  User.run_search_and_follow_task
end

desc "Scheduler will call this task daily to unfollow followed users"
task :unfollow_terms => :environment do
  User.run_unfollow_nonfollowers
end
