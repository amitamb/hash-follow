# require 'log4r'
# require 'log4r/outputter/syslogoutputter'
# require 'resque/failure/multiple'
# require 'resque/failure/redis'

# module Resque
#   module Failure
#     class Syslog < Base

#       # Define your logger, change as needed.
#       def logger
#         unless @logger
#           @logger = Log4r::Logger.new 'resque'
#           @logger.outputters = Log4r::SyslogOutputter.new 'resque',
#             :ident => 'resque', :logopt => 'foo', :facility => 'local'
#         end
#         @logger
#       end

#       # We are using a two line log here: Resume and backtrace follow.
#       def save
#         logger.error "#{worker} #{queue} #{payload} #{exception.class}"
#         logger.error "#{exception} -- #{exception.backtrace.join('\n')}"
#       end
#     end
#   end
# end

# # Use multiple failure backends (Redis and Syslog)
# Resque::Failure::Multiple.classes = [Resque::Failure::Redis, Resque::Failure::Syslog]
# Resque::Failure.backend = Resque::Failure::Multiple


require "resque/tasks"

task "resque:setup" => :environment do
  ENV['QUEUE'] = '*'
  Resque.after_fork = Proc.new { ActiveRecord::Base.establish_connection }
end
task "jobs:work" => "resque:work"