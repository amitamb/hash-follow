class CreateFollowings < ActiveRecord::Migration
  def change
    create_table :followings do |t|
      t.string :screen_name
      t.text :user_data
      t.text :tweet
      t.references :term
      t.integer :term_run_at

      t.timestamps
    end
    add_index :followings, :term_id
  end
end
