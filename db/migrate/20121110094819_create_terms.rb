class CreateTerms < ActiveRecord::Migration
  def change
    create_table :terms do |t|
      t.string :name
      t.integer :interval
      t.references :user
      t.integer :max_to_follow
      t.boolean :notify, :default => true
      t.integer :followings_count, :default => 0

      t.timestamp :last_run_at

      t.timestamps
    end
    add_index :terms, :user_id
  end
end
