class AddIsDisabledToTerms < ActiveRecord::Migration
  def change
    add_column :terms, :is_disabled, :boolean, :default => false
  end
end
