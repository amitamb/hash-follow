class AddIsCanceledToFollowings < ActiveRecord::Migration
  def change
    add_column :followings, :is_canceled, :boolean, :default => false
  end
end
